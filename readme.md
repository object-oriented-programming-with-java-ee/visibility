# Visibility modifiers

This project demonstrates the syntax and use of visibility modifiers.

Not every combination is demonstrated, but this project is a good place for
you to start experimenting with.

|                                  | Default | Public | Private | Protected |
| -------------------------------- | ------- | ------ | ------- | --------- |
| Same class                       |    Y    |   Y    |   Y     |    Y      |
| Subclass in same package         |    Y    |   Y    |   N     |    Y      |
| Other class in same package      |    Y    |   Y    |   N     |    Y      |
| Subclass in different package    |    N    |   Y    |   N     |    Y      |
| Other class in different package |    N    |   Y    |   N     |    N      |