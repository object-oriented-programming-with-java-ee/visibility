package example;

import demo.Dog;

public class Postman {
	
	public void examineDog(Dog dog) {
		
		// default visibility
		// System.out.println("The dogs age is " + dog.age);
		System.out.println("I do not have access to default visibility of classes in other packages");
		
		// public visibility
		System.out.println("The collar says the dogs name is " + dog.nameOnCollar);
		
		// protected visibility
		System.out.println("I do not have access to protected fields because I am in a different package");
		
		// private visibility
		// The next line does not work because the subclass does not have
		// access to private fields in the parent
		// System.out.println("That dog is thinking about " + thinkingAbout);
		System.out.println("I don't have access to private fields on any other class");
	}
	
}
