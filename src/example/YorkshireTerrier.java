package example;

import demo.Dog;

// subclass, different package
public class YorkshireTerrier extends Dog {
	
	public void showMyDetails() {
		
		// default visibility
		// This class is in a different package and default visibility does
		// not allow access across packages so the next line does not work
		// System.out.println("My age is " + age);
		System.out.println("I do not have access to default fields in a different package");
		
		// public visibility
		System.out.println("My collar says my name is " + nameOnCollar);
		
		// protected visibility
		if (isHungry) {
			System.out.println("I am hungry");
		} else {
			System.out.println("I am not hungry");
		}
		
		// private visibility
		// The next line does not work because the subclass does not have
		// access to private fields in the parent
		// System.out.println("Right now I am thinking about " + thinkingAbout);
		System.out.println("I do not have access to private fields of my parent");
	}
	
}
