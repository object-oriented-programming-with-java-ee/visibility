package main;

import demo.Cat;
import demo.GermanShepherd;
import example.Postman;
import example.YorkshireTerrier;

public class Main {

	public static void main(String[] args) {

		System.out.println("---- showing a subclass in the same package ---");
		GermanShepherd rover = new GermanShepherd();
		rover.showMyDetails();		
		
		System.out.println("---- showing subclass in a different package ---");
		YorkshireTerrier fluffy = new YorkshireTerrier();
		fluffy.showMyDetails();		
		
		System.out.println("---- expose methods to allow interacting with private fields ----");
		String thinkingAbout = rover.getThoughts();
		System.out.println("Rover is thinking about " + thinkingAbout);
		
		System.out.println("---- other class in the same package");
		Cat cat = new Cat();
		cat.examineDog(rover);
		
		System.out.println("---- other class in a different package");
		Postman pat = new Postman();
		pat.examineDog(rover);
	}

}
