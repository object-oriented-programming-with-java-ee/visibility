package demo;

// subclass, same package
public class GermanShepherd extends Dog {
	
	public void showMyDetails() {
		
		// default visibility
		System.out.println("My age is " + age);
		
		// public visibility
		System.out.println("My collar says my name is " + nameOnCollar);
		
		// protected visibility
		if (isHungry) {
			System.out.println("I am hungry");
		} else {
			System.out.println("I am not hungry");
		}
		
		// private visibility
		// The next line does not work because the subclass does not have
		// access to private fields in the parent
		// System.out.println("Right now I am thinking about " + thinkingAbout);
		System.out.println("I do not have access to private fields of my parent");
	}
	
}
