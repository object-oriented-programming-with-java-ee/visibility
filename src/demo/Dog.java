package demo;

public class Dog implements Animal {

	// Default visibility
	Integer age = 4;
	
	// Public visibility
	public String nameOnCollar = "Sir Barksalot";
	
	// Protected visibility
	protected Boolean isHungry = true;
	
	// Private visibility
	private String thinkingAbout = "Chasing the postman";
	
	@Override
	public void makeNoise() {
		System.out.println("Woof");
	}
	
	public String getThoughts() {
		// methods in the same class have access to private fields
		return thinkingAbout;
	}
	
}
