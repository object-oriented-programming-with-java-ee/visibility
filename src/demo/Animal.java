package demo;

public interface Animal {

	// interface methods are always public
	public void makeNoise();
	
}
