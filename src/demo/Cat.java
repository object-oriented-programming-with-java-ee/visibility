package demo;

public class Cat {

	public void examineDog(Dog dog) {
		
		// default visibility
		System.out.println("The dogs age is " + dog.age);
		
		// public visibility
		System.out.println("The collar says the dogs name is " + dog.nameOnCollar);
		
		// protected visibility
		if (dog.isHungry) {
			System.out.println("That dog looks hungry");
		} else {
			System.out.println("That dog doesn't look hungry");
		}
		
		// private visibility
		// The next line does not work because the subclass does not have
		// access to private fields in the parent
		// System.out.println("That dog is thinking about " + thinkingAbout);
		System.out.println("I don't have access to private fields on any other class");
	}
	
}
